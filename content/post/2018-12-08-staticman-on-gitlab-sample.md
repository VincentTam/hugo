---
title: Staticman on GitLab Sample
subtitle: Static comments on GitLab Pages
date: 2018-12-08
bigimg: [{src: "img/staticmanlab-hugo.svg", desc: "Hugo Staticman Logo"}]
---

Beautiful Hugo adds native [Staticman][1] support created by [Bruno Adele][2].
[Staticman][1] adds native GitLab support written by [Nicolas Tsim][3].

### Basic setup

1. Clone this repo.

        git clone https://gitlab.com/pages/hugo.git <your-site-name>

2. Remove existing comments under the folder `data` and the forking relationship.  (Otherwise, `.gitlab-ci.yml` won't start.)
3. Apart from the usual config parameters, modify the following fields
responsible for [Staticman][1] in `config.toml`. You may comment out stuff by
`#`.

        [Params.staticman]
          api = "https://staticman3.herokuapp.com/v3/entry/gitlab/<username>/<username>.gitlab.io/master/comments"
          pulls = "https://gitlab.com/<username>/<username>.gitlab.io/merge_requests"
4. Invite [@staticmanlab][4] as a collaborator with *developer* access.

### Optional features

- Instant regeneration: allow "maintainers + developers" to push to protected
branch in the GitLab repo's config, and set `moderation` to false in
`staticman.yml`.
- reCAPTCHA: new users have to apply for a new site key for each site, and then
edit the section `[Params.staticman.recaptcha]` with their proper site key and
site secret *encrypted through the* `/v3/encrypt` endpoint.

[1]: https://staticman.net/
[2]: http://bruno.adele.im/
[3]: https://gitlab.com/ntsim
[4]: https://gitlab.com/staticmanlab
